FROM ubuntu:18.04 AS basexilinx

LABEL maintainer="Antonio Delgado"

RUN dpkg --add-architecture i386

RUN apt update && \
    DEBIAN_FRONTEND=noninteractive apt install --no-install-recommends -y -q \
    build-essential \
    sudo \
    tofrodos \
    iproute2 \
    gawk \
    net-tools \
    expect \
    libncurses5-dev \
    tftpd \
    update-inetd \
    libssl-dev \
    flex \
    bison \
    libselinux1 \
    gnupg \
    wget \
    socat \
    gcc-multilib \
    libsdl1.2-dev \
    libglib2.0-dev \
    lib32z1-dev \
    libgtk2.0-0 \
    screen \
    pax \
    diffstat \
    xvfb \
    xterm \
    texinfo \
    gzip \
    unzip \
    cpio \
    chrpath \
    autoconf \
    lsb-release \
    libtool \
    libtool-bin \
    locales \
    kmod \
    git \
    rsync \
    bc \
    u-boot-tools \
    python \
    zlib1g:i386 \
    && apt clean \
    && rm -rf /var/lib/apt/lists/*

RUN locale-gen es_ES.UTF-8 && update-locale

FROM basexilinx

ARG PETA_VERSION
ARG PETA_RUN_FILE

# make a Vivado user
RUN adduser --disabled-password --gecos '' vivado && \
    usermod -aG sudo vivado && \
    echo "vivado ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

COPY accept-eula.sh ${PETA_RUN_FILE} /

# run the install
RUN chmod a+rx /${PETA_RUN_FILE} && \
    chmod a+rx /accept-eula.sh && \
    mkdir -p /opt/Xilinx && \
    chmod 777 /tmp /opt/Xilinx && \
    cd /tmp && \
    sudo -u vivado -i /accept-eula.sh /${PETA_RUN_FILE} /opt/Xilinx/petalinux && \
    rm -f /${PETA_RUN_FILE} /accept-eula.sh

# make /bin/sh symlink to bash instead of dash:
RUN echo "dash dash/sh boolean false" | debconf-set-selections
RUN DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash

USER vivado
ENV HOME /home/vivado
ENV LANG es_ES.UTF-8
RUN mkdir /home/vivado/project
WORKDIR /home/vivado/project

# add vivado tools to path
RUN echo "source /opt/Xilinx/petalinux/settings.sh" >> /home/vivado/.bashrc
