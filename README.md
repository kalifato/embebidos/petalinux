# Petalinux for docker

This repo contains methods/scripts to create your petalinux docker image.

## Create docker images

Copy your desired petalinux installer, as example a 2020.3 version it's used, file to this folder. Then run:

`docker build --build-arg PETA_VERSION=2020.3 --build-arg PETA_RUN_FILE=petalinux-v2020.3-final-installer.run -t petalinux:2020.3 .`

## Usage petalinux

After installation, launch petalinux with:

`docker run -ti --rm -e DISPLAY=$DISPLAY --net="host" -v /tmp/.X11-unix:/tmp/.X11-unix -v $HOME/.Xauthority:/home/vivado/.Xauthority -v $HOME/Projects:/home/vivado/project  petalinux:2020.3 /bin/bash`

